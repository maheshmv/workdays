﻿using System;
using System.Collections.Generic;
using Xunit;
using Moq;
using WorkDaysAPI.Source.Handler;
using WorkDaysAPI.Source;
using WorkDaysAPI.Source.Interfaces;

namespace WorkDays.Tests
{
    public class DateHandlerTests
    {
        Mock<IHolidaysHandler> MockHolidayHandler;
        IDateHandler dateHandler; 

        List<DateTime> mockHolidayHandler = new List<DateTime>
        {
            new DateTime(2020,1,1),
            new DateTime(2020,4,25),
            new DateTime(2020,6,14),
            new DateTime(2020,10,8),
            new DateTime(2020,12,25),
            new DateTime(2020,12,28),
            new DateTime(2019,1,1),
            new DateTime(2019,4,25),
            new DateTime(2019,6,14),
            new DateTime(2019,10,4),
            new DateTime(2019,12,25),
            new DateTime(2019,12,28),
            new DateTime(2018,1,1),
            new DateTime(2018,4,25),
            new DateTime(2018,6,14),
            new DateTime(2018,10,4),
            new DateTime(2018,12,25),
            new DateTime(2018,12,28),
            new DateTime(2017,1,1),
            new DateTime(2017,4,25),
            new DateTime(2017,6,14),
            new DateTime(2017,10,4),
            new DateTime(2017,12,25),
            new DateTime(2017,12,28),
        };
        private void Setup()
        {
            MockHolidayHandler = new Mock<IHolidaysHandler>();
            
            MockHolidayHandler.Setup(x => x.GetWeekDayPublicHolidaysYearly(It.IsAny<DateTime>(), It.IsAny<DateTime>()))
                .Returns(mockHolidayHandler);
            dateHandler = new DateHandler(MockHolidayHandler.Object);
        }
        [Fact]
        public void WorkingForJanuary()
        {
            Setup();
            DateTime start = new DateTime(2018, 1, 1);
            DateTime end = new DateTime(2018, 1, 1);

            int WorkingDayHolidays = 0;  
            int numHolidays = dateHandler.GetNumberOfWorkingDays(start, end);

            Assert.Equal(WorkingDayHolidays, numHolidays);


            start = new DateTime(2018, 1, 1);
            end = new DateTime(2018, 1, 1);
            WorkingDayHolidays = 0;
            numHolidays = dateHandler.GetNumberOfWorkingDays(start, end);

            Assert.Equal(WorkingDayHolidays, numHolidays);

            start = new DateTime(2018, 1, 1);
            end = new DateTime(2018, 1, 31);
            WorkingDayHolidays = 21;
            numHolidays = dateHandler.GetNumberOfWorkingDays(start, end);

            Assert.Equal(WorkingDayHolidays, numHolidays);
        }
        [Fact]
        public void WorkingForInvalidDateRange()
        {
            Setup();
            DateTime start = new DateTime(2020, 11, 4); 
            DateTime end = new DateTime(2020, 10, 23);

            int WorkingDayHolidays = 0;
            int numHolidays = dateHandler.GetNumberOfWorkingDays(start, end);

            Assert.Equal(WorkingDayHolidays, numHolidays);
        }
        [Fact]
        public void WorkingForExtremeDateValue()
        {
            Setup();
            DateTime start = new DateTime(2020, 11, 4); 
            DateTime end = DateTime.MaxValue;

            int WorkingDayHolidays = 0;
            int numHolidays = dateHandler.GetNumberOfWorkingDays(start, end);

            Assert.Equal(WorkingDayHolidays, numHolidays);

            end = new DateTime(2020, 11, 4); 
            start = DateTime.MinValue;

            numHolidays = dateHandler.GetNumberOfWorkingDays(start, end);

            Assert.Equal(WorkingDayHolidays, numHolidays);
        }
        [Fact]
        public void WorkingForTwoDatesInAYear()
        {
            Setup();
            DateTime start = new DateTime(2020, 3, 4); // 4th March
            DateTime end = new DateTime(2020, 10, 23);

            int WorkingDayHolidays = 165;
            int numHolidays = dateHandler.GetNumberOfWorkingDays(start, end);

            Assert.Equal(WorkingDayHolidays, numHolidays);
        }
        [Fact]
        public void WorkingForTwoDatesInThreeYears()
        {
            Setup();
            DateTime start = new DateTime(2017, 5, 17);
            DateTime end = new DateTime(2019, 12, 13);

            int WorkingDayHolidays = 657;
            int numHolidays = dateHandler.GetNumberOfWorkingDays(start, end);

            Assert.Equal(WorkingDayHolidays, numHolidays);
        }
        [Fact]
        public void WorkingForTwoDatesInAFullWorkingWeek()
        {
            Setup();
            DateTime start = new DateTime(2020, 5, 11);
            DateTime end = new DateTime(2020, 5, 15);

            int WorkingDayHolidays = 3; // Start , end dates ignored
            int numHolidays = dateHandler.GetNumberOfWorkingDays(start, end);

            Assert.Equal(WorkingDayHolidays, numHolidays);
        }
        [Fact]
        public void WorkingForTwoDatesInAWeekWithPublicHolidayOnWeekDay()
        {
            Setup();
            DateTime start = new DateTime(2020, 12, 24);
            DateTime end = new DateTime(2020, 12, 31);

            int WorkingDayHolidays = 2; // only 29,30 are counted
            int numHolidays = dateHandler.GetNumberOfWorkingDays(start, end);

            Assert.Equal(WorkingDayHolidays, numHolidays);
        }
        [Fact]
        public void WorkingForTwoLongDates()
        {
            Setup();
            DateTime start = new DateTime(1900, 12, 24);
            DateTime end = new DateTime(2020, 12, 31);

            int WorkingDayHolidays = 31292; 
            int numHolidays = dateHandler.GetNumberOfWorkingDays(start, end);

            Assert.Equal(WorkingDayHolidays, numHolidays);
            
        }

    }
}
