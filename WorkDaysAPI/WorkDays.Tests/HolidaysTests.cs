using System;
using System.Collections.Generic;
using Xunit.Extensions;
using Xunit;
using Xunit.Sdk;
using Moq;
using WorkDaysAPI.Source.Interfaces;
using Microsoft.Extensions.Configuration;
using WorkDaysAPI.Source.Handler;
using WorkDaysAPI.Source;

namespace WorkDays.Tests
{
    public class HolidaysTests
    {
        IHolidaysHandler holidaysHandler = new HolidaysHandler();
        private List<HolidaysModel> _holidayModel = new List<HolidaysModel>
        {
                new HolidaysModel {Day=26,Month=1,Type=FlexibilityType.Fixed,Name="Holiday1"},
                new HolidaysModel {Day=25,Month=4,Type=FlexibilityType.Special,Name="Holiday2"},
                new HolidaysModel {Day=06,Month=1,Type=FlexibilityType.Flexible,Name="Holiday3"},
                new HolidaysModel {Day=09,Month=5,Type=FlexibilityType.Fixed,Name="Holiday4"}
        };
        [Fact]
        public void TestHolidaysOnWorkingDaysInAYear()
        {

            DateTime start = new DateTime(2020, 4, 1);
            DateTime end = new DateTime(2020, 6, 30);

            int WorkingDayHolidays = 5;

            List<DateTime> holidays = holidaysHandler.GetWeekDayPublicHolidaysYearly(start, end);

            Assert.Equal(WorkingDayHolidays, holidays.Count);
        }
        [Fact]
        public void TestHolidaysOnWorkingDaysInTwoYears()
        {

            DateTime start = new DateTime(2020, 4, 1);
            DateTime end = new DateTime(2021, 6, 30);

            int WorkingDayHolidays = 9;

            List<DateTime> holidays = holidaysHandler.GetWeekDayPublicHolidaysYearly(start, end);

            Assert.Equal(WorkingDayHolidays, holidays.Count);
        }
        [Fact]
        public void TestHolidaysOnWorkingDaysFor1stJan()
        {

            DateTime start = new DateTime(2018, 1, 1);
            DateTime end = new DateTime(2018, 1, 1);

            int WorkingDayHolidays = 6;  // holidays in 2018

            List<DateTime> holidays = holidaysHandler.GetWeekDayPublicHolidaysYearly(start, end);

            Assert.Equal(WorkingDayHolidays, holidays.Count);


            start = new DateTime(2017, 1, 1);
            end = new DateTime(2018, 1, 1); // holidays in 2017, 2018
            WorkingDayHolidays = 12;
            holidays = holidaysHandler.GetWeekDayPublicHolidaysYearly(start, end);

            Assert.Equal(WorkingDayHolidays, holidays.Count);
        }
    }
}
