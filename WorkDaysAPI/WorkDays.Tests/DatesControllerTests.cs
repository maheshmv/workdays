﻿using System;
using System.Collections.Generic;
using WorkDaysAPI.Source;
using WorkDaysAPI.Controllers;
using WorkDaysAPI.Source.Interfaces;
using Moq;
using Xunit;

namespace WorkDays.Tests
{
    public class DatesControllerTests
    {
        Mock<IDatesDiff> MockDateDiff;
        DatesController controller;
        private void Setup()
        {
            MockDateDiff  = new Mock<IDatesDiff>();
            MockDateDiff.Setup(x => x.CalculateDiff(It.IsAny<DateTime>(), It.IsAny<DateTime>()))
                .Returns(100);
            controller = new DatesController(MockDateDiff.Object);
        }
        [Fact]
        public void InvalidStringasDate()
        {
            Setup();
            string date1 = "2018-13-04"; //yyyy-mm-dd
            string date2 = "2018-12-26";

            //Assert.Throws<FormatException>(() => controller.GetDiffCalculator(date1, date2));
            DatesController.MyResponse response = controller.GetDiffCalculator(date1, date2);

            Assert.NotNull(response.Error);
            Assert.Equal(System.Net.HttpStatusCode.NotFound,response.result);

        }
        [Fact]
        public void OddValidDate()
        {
            Setup();
            string date1 = "201-01-04"; //yyyy-mm-dd
            string date2 = "2018-12-26";
            DatesController.MyResponse ret = controller.GetDiffCalculator(date1, date2);

            Assert.Equal("None", ret.Error);
            Assert.Equal(System.Net.HttpStatusCode.OK, ret.result);
        }
        [Fact]
        public void ValidDate()
        {
            Setup();
            string date1 = "2018-07-04"; //yyyy-mm-dd
            string date2 = "2018-12-26";
            DatesController.MyResponse ret = controller.GetDiffCalculator(date1, date2);

            Assert.Equal("None", ret.Error);
            Assert.Equal(100, ret.Days);
            Assert.Equal(System.Net.HttpStatusCode.OK, ret.result);
        }
    }
}
