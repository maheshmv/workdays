﻿using System;
using Newtonsoft.Json;  
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http;
using WorkDaysAPI.Source.Interfaces;
using WorkDaysAPI.Source;
using Microsoft.AspNetCore.Mvc;

namespace WorkDaysAPI.Controllers
{
    //[Route("api/[controller]")]
    public class DatesController : ApiController
    {

        public class MyResponse{
            public HttpStatusCode result { get; set; }
            public  int Days { get; set; }
            public string Error { get; set; }
        }
        IDatesDiff diffCalculator;
        public DatesController(IDatesDiff _datesDiff)
        {
            diffCalculator = _datesDiff;
        }
        public MyResponse GetDiffCalculator(string startDate, string endDate)
        {
            int ret = 0;
            bool flag = true;
            string error = null ;
            
            try
            {
                DateTime dtStartDate = Convert.ToDateTime(startDate);
                DateTime dtEndDate = Convert.ToDateTime(endDate);
                ret = diffCalculator.CalculateDiff(dtStartDate, dtEndDate);
            }
            catch (FormatException ex)
            {
                error = ex.Message+ "; Please enter valid Startdate and EndDate e.g. /Dates/<start date  yyyy-mm-dd>/<end date  yyyy-mm-dd>";
                flag = false;
            }
            catch (Exception ex)
            {
                error = ex.Message;
                flag = false;
            }
            finally
            {
            }


            if (flag)
                return new MyResponse { Days = ret, result = HttpStatusCode.OK,Error="None" };
            else
                return new MyResponse { Error = error, result = HttpStatusCode.NotFound };
        }

        public MyResponse DateError(string date)
        {
            HttpResponseMessage ret = new HttpResponseMessage(HttpStatusCode.BadRequest);

            return new MyResponse { Error = "Wrong address. Please endter both start and end dates; e.g. /Dates/<start date yyyy-mm-dd>/<end date  yyyy-mm-dd>", result = HttpStatusCode.NotFound ,};
        }
       
    }
}
