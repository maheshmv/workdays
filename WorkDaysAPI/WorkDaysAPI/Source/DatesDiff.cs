﻿using System;
using System.Collections.Generic;
using System.Linq;
using WorkDaysAPI.Source.Interfaces;
using WorkDaysAPI.Source.Handler;

namespace WorkDaysAPI.Source
{
    public class DatesDiff : IDatesDiff
    {
        IDateHandler dateHandler;
        public DatesDiff(IDateHandler _dateHandler)
        {
            dateHandler = _dateHandler;
        }
        public int CalculateDiff(DateTime dateFrom, DateTime dateTo)
        { 
            int ret = 0;
            try
            {
                ret = dateHandler.GetNumberOfWorkingDays(dateFrom, dateTo);
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return ret;
        }

        public void Dispose()
        {
            
        }
    }
}
