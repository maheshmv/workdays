﻿using System;
using System.Collections.Generic;
using WorkDaysAPI.Source.Interfaces;


namespace WorkDaysAPI.Source.Handler
{

    public class DateHandler:IDateHandler
    {
        IHolidaysHandler holidaysHandler;
        public DateHandler(IHolidaysHandler _holidayHandler)
        {
            holidaysHandler = _holidayHandler;
        }
        public int GetNumberOfWorkingDays(DateTime startDate, DateTime lastDate)
        {
            if (DateTime.Compare(startDate, lastDate) > 0) return 0;
            if (startDate == DateTime.MinValue || startDate == DateTime.MaxValue ||
                lastDate == DateTime.MinValue || lastDate == DateTime.MaxValue)
                return 0;

            int days = 0;
            DateTime start = startDate.AddDays(1);
            DateTime end = lastDate;//.AddDays(-1);
            List<DateTime> holidays;
            try
            {
                holidays = holidaysHandler.GetWeekDayPublicHolidaysYearly(start, end);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            while (start < lastDate)
            {
                if (start.DayOfWeek != DayOfWeek.Saturday 
                        && start.DayOfWeek != DayOfWeek.Sunday
                        && !holidays.Contains(start))
                {
                    ++days;
                }
                start = start.AddDays(1);
            }
            return days;
        }
    }
}
