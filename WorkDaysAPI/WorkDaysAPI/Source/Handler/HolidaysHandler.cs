﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Extensions.Configuration;
using WorkDaysAPI.Source.Interfaces;

namespace WorkDaysAPI.Source.Handler
{
    public class HolidaysHandler : IHolidaysHandler
    {
        private  HolidaysModel[] _holidaysSettings;

        private static Dictionary<int, List<DateTime>> CachedYearlyHolidaysList = new Dictionary<int, List<DateTime>>();
        public  HolidaysHandler()
        {

            IConfiguration config = new ConfigurationBuilder()
                       .SetBasePath(Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory))
                       .AddJsonFile("holidaySettings.json", true, true)
                       .Build();

            _holidaysSettings = config.GetSection("PublicHolidaysStandard").Get<HolidaysModel[]>();
            if (_holidaysSettings == null || !ValidateHolidays(_holidaysSettings.ToList()))
            {
                string str = String.Format(" Invalid public holiday, Please check holiday settings file");
                throw new Exception(str);
            }
        }

        public List<DateTime> GetWeekDayPublicHolidaysYearly(DateTime start, DateTime end)
        {
            List<DateTime> retHolidayListonWeekDay = new List<DateTime>();
            try
            {
                CalculateHolidaysForPeriod(start, end);
                int yearIter = start.Year;
                while (yearIter <= end.Year)
                {
                    retHolidayListonWeekDay.AddRange(CachedYearlyHolidaysList[yearIter]);
                    yearIter++;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return retHolidayListonWeekDay;
        }
        private int CalculateHolidaysForPeriod(DateTime start, DateTime end)
        {
            DateTime curr = start;
            int nTotalYearsToCalculate = end.Year - start.Year;

            List<DateTime> _holidays = null;
            int yearIter = start.Year;
            try
            {
                while (yearIter <= end.Year)
                {
                    if (!CachedYearlyHolidaysList.ContainsKey(yearIter))
                    {
                        _holidays = GetWeekdayHolidaysOfYear(yearIter, _holidaysSettings);
                        CachedYearlyHolidaysList.Add(yearIter, _holidays); // Cache the value for next time usage.
                    }
                    yearIter++;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return 0;
        }
        private List<DateTime> GetWeekdayHolidaysOfYear(int year, HolidaysModel[] holidayDates)
        {
            List<DateTime> holidays = new List<DateTime> { };

            foreach (HolidaysModel publicHoliday in holidayDates)
            {
                DateTime currHoliday = DateTime.MinValue;
                try
                {
                    if (publicHoliday.Day != 0 && publicHoliday.Month != 0)
                    {

                        currHoliday = new DateTime(year, publicHoliday.Month, publicHoliday.Day);
                    }
                }
                catch(Exception ex)
                {
                    string str = String.Format("{0} -  Invalid public holiday date {1}/{2}/{3}",ex.Message, publicHoliday.Day,publicHoliday.Month,year);
                    throw new Exception(str);
                }

                if (publicHoliday.Type == FlexibilityType.Fixed
                                    && currHoliday != DateTime.MinValue)
                {
                    if (currHoliday.DayOfWeek != DayOfWeek.Saturday && currHoliday.DayOfWeek != DayOfWeek.Sunday)
                    {
                        holidays.Add(currHoliday);
                    }
                }
                else if (publicHoliday.Type == FlexibilityType.Flexible
                                    && currHoliday != DateTime.MinValue)
                {
                    if (currHoliday.DayOfWeek == DayOfWeek.Saturday)
                    {
                        holidays.Add(currHoliday.AddDays(2));
                    }
                    else if (currHoliday.DayOfWeek == DayOfWeek.Sunday)
                    {
                        holidays.Add(currHoliday.AddDays(1));
                    }
                    else
                    {
                        holidays.Add(currHoliday);
                    }
                }
                else if (publicHoliday.Type == FlexibilityType.Special
                                    && currHoliday != null)
                {
                    currHoliday = ConvertToPublicHolidayDate(publicHoliday.Month, publicHoliday.DayIndex, publicHoliday.DayOfWeek, year);
                    holidays.Add(currHoliday);
                }
            }
            //ValidateHolidays(holidays);
            return holidays;
        }
        private DateTime ConvertToPublicHolidayDate(int month, int dIndex, int dowIndex, int year)
        {
            DayOfWeek dow = (DayOfWeek)dowIndex;
            DateTime day = new DateTime(year, month, 1);

            int Nth = 0;
            DateTime dateIter = day;
            while (Nth < dIndex)
            {
                if (Nth != 0)
                {
                    dateIter = dateIter.AddDays(7);
                }
                else
                {
                    while (dateIter.DayOfWeek != dow)
                    {
                        dateIter = dateIter.AddDays(1);
                    }
                }
                Nth++;
            }
            return dateIter;
        }

        private bool ValidateHolidays(List<HolidaysModel> pubHolidays)
        {
            foreach(HolidaysModel holiday in pubHolidays)
            {
                if (holiday.Type == FlexibilityType.Fixed ||
                    holiday.Type == FlexibilityType.Flexible)
                {
                    if (holiday.Day < 1 || holiday.Day > 31
                        || holiday.Month < 1 || holiday.Month > 12
                        || holiday.Name == null)
                    {
                        return false;
                    }
                }
                else if (holiday.Type == FlexibilityType.Special)
                {
                    if (holiday.DayIndex < 1 || holiday.DayIndex > 31
                        || holiday.DayOfWeek < 0 || holiday.DayOfWeek > 6
                        || holiday.Month < 1 || holiday.Month > 12
                        || holiday.Name == null)
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
                
            }

            return true;
        }
    }
}
