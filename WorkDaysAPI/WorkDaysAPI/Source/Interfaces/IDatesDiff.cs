﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkDaysAPI.Source.Interfaces
{
    public interface IDatesDiff:IDisposable
    {
        int CalculateDiff(DateTime dateFrom, DateTime dateTo);
    }
}
