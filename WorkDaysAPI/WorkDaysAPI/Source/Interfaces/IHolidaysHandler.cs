﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkDaysAPI.Source.Interfaces
{
    public interface IHolidaysHandler
    {
        List<DateTime> GetWeekDayPublicHolidaysYearly(DateTime start, DateTime end);
    }
}
