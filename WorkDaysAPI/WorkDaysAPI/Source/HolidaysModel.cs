﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkDaysAPI.Source
{

    public enum FlexibilityType
    {
        Fixed = 0,
        Flexible,
        Special
    }

    public class HolidaysModel
    {
        public int Month { get; set; }
        public int Day { get; set; }
        public int DayOfWeek { get; set; }
        public int DayIndex { get; set; }
        public string Name { get; set; }
        public FlexibilityType Type { get; set; }

    }
}
