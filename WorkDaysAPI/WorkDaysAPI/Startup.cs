﻿using System;
using System.Collections.Generic;
using System.Linq;
using WorkDaysAPI.Source.Interfaces;
using WorkDaysAPI.Source.Handler;
using WorkDaysAPI.Source;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc.Abstractions;

namespace WorkDaysAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options =>
            {
                options.RespectBrowserAcceptHeader = true; // false by default
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddScoped<IHolidaysHandler, HolidaysHandler>();
            services.AddScoped<IDateHandler, DateHandler>();
            services.AddScoped<IDatesDiff, DatesDiff>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();    
            }
            app.UseMvc(routes =>
            {
                routes.MapRoute("both", "{controller}/{startDate}/{endDate}", defaults: new { controller = "Dates", action = "GetDiffCalculator" });
                routes.MapRoute("start", "{controller}/{startDate}/", defaults: new { controller = "Dates", action = "DateError" });
                routes.MapRoute("end", "{controller}/{endDate}/", defaults: new { controller = "Dates", action = "DateError" });
                routes.MapRoute("default", "{controller=Dates}/{action=DateError}/{startDateStr?}");
            });

            //app.UseMvc();
        }
    }
}
