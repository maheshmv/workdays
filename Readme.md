**Clone**

ssh - git clone git@bitbucket.org:maheshmv/workdays.git 

http -   git clone https://maheshmv@bitbucket.org/maheshmv/workdays.git  

**Restore packages**

Open the Solution file WorkDaysAPI.sln.

Build the project using the Ctrl + Shift + B to download the Nuget packages.

Run the application using Ctrl + F5.

Enter http://localhost:60903/Dates/<start date  yyyy-mm-dd>/<end date  yyyy-mm-dd> in browser to get the workdays between two dates. e.g. http://localhost:60903/Dates/2018-04-21/2018-05-20



